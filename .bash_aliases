alias diff='colordiff'
alias egrep='egrep --color=auto'
alias fgrep='fgrep'
alias grep='grep'
alias ls='ls -la --color=auto'
alias ll='ls -la --color=auto'
alias la='ls -a --color=auto'
alias lh='ls -lisAd .[^.]*'
alias rm='rm -iv'
alias hs='history|grep -i --color=auto'
alias ff='find . -type f -iname'
alias c='clear'
## get rid of command not found ##
alias cd..='cd ..'
## a quick way to get out of current directory ##
alias ..='cd ..'
alias ...='cd ../../../'
alias ....='cd ../../../../'
alias .....='cd ../../../../'
alias .4='cd ../../../../'
alias .5='cd ../../../../..'

# top
alias cpu='top -o cpu'
alias mem='top -o rsize' # memory

alias vi="vim"
