# setup shell
touch /root/.bashrc
echo "export CLICOLOR=1" >> /root/.bashrc
echo "export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx" >> /root/.bashrc
echo "export PS1='\[\033[36m\]\u\[\033[m\]@\[\033[33;1m\]\w\[\033[m\]\$ '" >> /root/.bashrc
echo "export EDITOR=vim" >> /root/.bashrc
echo "if [ -f ~/.bash_aliases ]; then" >> /root/.bashrc
echo "    . ~/.bash_aliases" >> /root/.bashrc
echo "fi" >> /root/.bashrc
touch /root/.bash_history
touch /root/.vimrc
echo "syntax on" >> /root/.vimrc

wget https://bitbucket.org/dreed477/shell-setup/raw/master/.bash_aliases -O /root/.bash_aliases 

# set up shell 
#RUN wget "https://bitbucket.org/dreed477/shell-setup/raw/master/setup.sh" -O setup.sh && chown root setup.sh && chmod +x setup.sh && ./setup.sh && rm -rvf setup.sh
